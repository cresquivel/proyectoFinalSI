package model.entities;

import java.util.List;

public class Instructor {
	
	private String ID;
	private String name;
	private Department department;
	private Double salary;
	private List<Section> sections;
	
	private boolean isDeleted;
	
	public  Instructor()
	{
		this.isDeleted= false;
	}
	
	
	public Instructor(String iD, String name, Department department, Double salary, List<Section> sections,
			boolean isDeleted) {
		
		ID = iD;
		this.name = name;
		this.department = department;
		this.salary = salary;
		this.sections = sections;
		this.isDeleted = isDeleted;
	}


	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Department getDepartment() {
		return department;
	}
	public void setDepartment(Department department) {
		this.department = department;
	}
	public Double getSalary() {
		return salary;
	}
	public void setSalary(Double salary) {
		this.salary = salary;
	}
	public List<Section> getSections() {
		return sections;
	}
	public void setSections(List<Section> sections) {
		this.sections = sections;
	}


	public boolean isDeleted() {
		return isDeleted;
	}


	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	

}

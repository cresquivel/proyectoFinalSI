package model.entities;

import java.util.List;

public class Classroom {
	
	private String building;
	private String room_number;
	private int capacity;
	private List<Section> sections;
	
	
	
	private boolean isDeleted;
	
	
	public  Classroom()
	{
		this.isDeleted= false;
	}
	
	
	public Classroom(String building, String room_number, int capacity, List<Section> sections, boolean isDeleted) {
		super();
		this.building = building;
		this.room_number = room_number;
		this.capacity = capacity;
		this.sections = sections;
		this.isDeleted = isDeleted;
	}


	public String getBuilding() {
		return building;
	}
	public void setBuilding(String building) {
		this.building = building;
	}
	public String getRoom_number() {
		return room_number;
	}
	public void setRoom_number(String room_number) {
		this.room_number = room_number;
	}
	public int getCapacity() {
		return capacity;
	}
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	public List<Section> getSections() {
		return sections;
	}
	public void setSections(List<Section> sections) {
		this.sections = sections;
	}


	public boolean isDeleted() {
		return isDeleted;
	}


	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	

}

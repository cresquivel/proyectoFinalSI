package model.entities;

public class Student {
	
	private String ID;
	private String name;
	private int tot_cred;
	private Department deparment;
	
	private boolean isDeleted;
	
	public  Student()
	{
		this.isDeleted= false;
	}
	
	public Student(String iD, String name, int tot_cred, Department deparment, boolean isDeleted) {
		ID = iD;
		this.name = name;
		this.tot_cred = tot_cred;
		this.deparment = deparment;
		this.isDeleted = isDeleted;
	}

	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getTot_cred() {
		return tot_cred;
	}
	public void setTot_cred(int tot_cred) {
		this.tot_cred = tot_cred;
	}
	public Department getDeparment() {
		return deparment;
	}
	public void setDeparment(Department deparment) {
		this.deparment = deparment;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	
	

}

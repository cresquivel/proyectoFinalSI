package model.entities;

import java.util.List;

public class Department {
	
	private String dept_name;
	private String building;
	private int budget;
	private List<Course> courses;
	private List<Instructor> instructors;
	private List<Student> students;
	
	private boolean isDeleted;
	
	public  Department()
	{
		this.isDeleted= false;
	}
	
 	
	public Department(String dept_name, String building, int budget, List<Course> courses, List<Instructor> instructors,
			List<Student> students, boolean isDeleted) {
		
		this.dept_name = dept_name;
		this.building = building;
		this.budget = budget;
		this.courses = courses;
		this.instructors = instructors;
		this.students = students;
		this.isDeleted = isDeleted;
	}


	public boolean isDeleted() {
		return isDeleted;
	}


	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}


	public String getDept_name() {
		return dept_name;
	}
	public void setDept_name(String dept_name) {
		this.dept_name = dept_name;
	}
	public String getBuilding() {
		return building;
	}
	public void setBuilding(String building) {
		this.building = building;
	}
	public int getBudget() {
		return budget;
	}
	public void setBudget(int budget) {
		this.budget = budget;
	}
	public List<Course> getCourses() {
		return courses;
	}
	public void setCourses(List<Course> courses) {
		this.courses = courses;
	}
	public List<Instructor> getInstructors() {
		return instructors;
	}
	public void setInstructors(List<Instructor> instructors) {
		this.instructors = instructors;
	}
	public List<Student> getStudents() {
		return students;
	}
	public void setStudents(List<Student> students) {
		this.students = students;
	}
	
	
	

}

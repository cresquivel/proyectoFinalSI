package model.entities;

import java.util.List;

public class Section {
	
	private Course course;
	private String sec_id;
	private String semester;
	private int year;
	private Classroom classroom;
	private String time_slot_id;
	private List<Instructor> instructors;
	
	private boolean isDeleted;
	
	
	public  Section()
	{
		this.isDeleted= false;
	}
	
	
	public Section(Course course, String sec_id, String semester, int year, Classroom classroom, String time_slot_id,
			List<Instructor> instructors, boolean isDeleted) {
		
		this.course = course;
		this.sec_id = sec_id;
		this.semester = semester;
		this.year = year;
		this.classroom = classroom;
		this.time_slot_id = time_slot_id;
		this.instructors = instructors;
		this.isDeleted = isDeleted;
	}


	public Course getCourse() {
		return course;
	}
	public void setCourse(Course course) {
		this.course = course;
	}
	public String getSec_id() {
		return sec_id;
	}
	public void setSec_id(String sec_id) {
		this.sec_id = sec_id;
	}
	public String getSemester() {
		return semester;
	}
	public void setSemester(String semester) {
		this.semester = semester;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public Classroom getClassroom() {
		return classroom;
	}
	public void setClassroom(Classroom classroom) {
		this.classroom = classroom;
	}
	public String getTime_slot_id() {
		return time_slot_id;
	}
	public void setTime_slot_id(String time_slot_id) {
		this.time_slot_id = time_slot_id;
	}
	public List<Instructor> getInstructors() {
		return instructors;
	}
	public void setInstructors(List<Instructor> instructors) {
		this.instructors = instructors;
	}


	public boolean isDeleted() {
		return isDeleted;
	}


	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	
	

}

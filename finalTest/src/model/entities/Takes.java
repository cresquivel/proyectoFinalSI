package model.entities;

public class Takes {
	
	private Student student;
	private Section section;
	private String grade;
	
	private boolean isDeleted;
	
	
	public  Takes()
	{
		this.isDeleted= false;
	}
	
	public Takes(Student student, Section section, String grade, boolean isDeleted) {
	
		this.student = student;
		this.section = section;
		this.grade = grade;
		this.isDeleted = isDeleted;
	}

	public Student getStudent() {
		return student;
	}
	public void setStudent(Student student) {
		this.student = student;
	}
	public Section getSection() {
		return section;
	}
	public void setSection(Section section) {
		this.section = section;
	}
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	

}
